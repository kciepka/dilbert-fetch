const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');
const moment = require('moment');

const args = process.argv.slice(2);
const daysBefore = Number.parseInt(args[0] || 0) + 1;

if (!fs.existsSync('downloaded')) {
    fs.mkdirSync('downloaded');
}

function getImage(day) {
    return new Promise((resolve, reject) => {
        axios.get(`https://dilbert.com/strip/${day}`)
            .then(function (response) {
                const $ = cheerio.load(response.data);
                const link = $('img.img-responsive.img-comic').attr('src');

                axios({
                    method: 'get',
                    headers: { 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.98 Chrome/71.0.3578.98 Safari/537.36' },
                    url: `https:${link}`,
                    responseType: 'stream'
                })
                    .then(function (response) {
                        console.log(`Downloading Dilbert strip from ${day}...`);
                        response.data.pipe(fs.createWriteStream(`downloaded/${day}.gif`))
                        setTimeout(() => {
                            resolve();
                        }, 3000)
                    })
                    .catch(err => {
                        console.error(err);
                    });
            })
            .catch(function (error) {
                console.error(error);
            });
    })
}

async function getAllImages() {
    console.log("Starting Dilbert strip download");
    console.log("*******************************");
    for (let i = 0; i < daysBefore - 1; i++) {
        const day = moment().subtract(i, 'days').format('YYYY-MM-DD');
        await getImage(day);
    }
}

getAllImages();